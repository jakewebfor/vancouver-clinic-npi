<?php

/**
 *
 * @link              https://webfor.com
 * @since             1.0.0
 * @package           Vancouver_Clinic_Npi
 *
 * @wordpress-plugin
 * Plugin Name:       NPI Ratings for Vancouver Clinic Providers
 * Plugin URI:        https://tvc.org
 * Description:       Displays Vancouver Clinic Provider NPI ratings provided by NRC Health. Display are locations defined in source code. 
 * Version:           1.0.0
 * Author:            Webfor
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       vancouver-clinic-npi
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*
* Enqueue Provider script based on Provider post type and NPI ID
*/
add_action('wp_enqueue_scripts', function(){
	$plugin_url = plugin_dir_url( __FILE__ );
	
	//Request for Providers
	if(is_singular('provider_post_type') && function_exists('get_field')){
		//If the ACF field exists, make the API request
		if(get_field('provider_npi')){
			$npi = get_field('provider_npi');
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL            => "https://transparency.nrchealth.com/widget/api/org-profile/vancouver-clinic/npi/{$npi}/",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING       => "",
				CURLOPT_MAXREDIRS      => 10,
				CURLOPT_TIMEOUT        => 30,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST  => "GET",
				CURLOPT_HTTPHEADER     => array(
					"Cache-Control: no-cache",
					"Postman-Token: 9b7e1774-eb06-4ca2-ac1c-0c95d9c71993"
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			$response = json_decode($response, true); 

			//Check for a valid response -- if valid, enqueue styles and scripts
			if ($response['valid']){
				
				wp_enqueue_script( 'provider-npi', $plugin_url  . 'js/provider-npi.js', array('jquery'), false, true);
			}

			//Make the Provider image available for schema
			$data = array(
				'image' => get_the_post_thumbnail_url(),
			);
			wp_localize_script( 'provider-npi', 'php_vars', $data );
		}

		
	}

	// Request for Services
	if(is_singular('service_post_type') && function_exists('get_field')){
		//If the ACF field exists, make the API request
		if(get_field('nrc_service_slug')){
			$service = get_field('nrc_service_slug');
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL            => "https://transparency.nrchealth.com/widget/api/org-group/vancouver-clinic/{$service}/",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING       => "",
				CURLOPT_MAXREDIRS      => 10,
				CURLOPT_TIMEOUT        => 30,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST  => "GET",
				CURLOPT_HTTPHEADER     => array(
					"Cache-Control: no-cache",
					"Postman-Token: 9b7e1774-eb06-4ca2-ac1c-0c95d9c71993"
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			$response = json_decode($response, true); 

			//Check for a valid response -- if valid, enqueue styles and scripts
			if ($response['valid']){
				
				wp_enqueue_script( 'service-group', $plugin_url  . 'js/service-group.js', array('jquery'), false, true);

				//Make the Service Slug accessible in the javascript object
				$data = array(
					'service' => $service,
				);
				wp_localize_script( 'service-group', 'php_vars', $data );
			}
		}

	}
	wp_enqueue_style( 'nrc', $plugin_url . 'css/nrc.css', '', false, 'screen' );
});

// Access the LOTW for each individual Provider or Service
add_action('wp_footer', function(){
	if(is_singular('provider_post_type') && function_exists('get_field')){
		if(get_field('provider_npi')){
			$npi = get_field('provider_npi');
			echo "<script src='https://transparency.nrchealth.com/widget/v2/vancouver-clinic/npi/{$npi}/lotw.js' async></script>";
		}
	}

	if(is_singular('service_post_type') && function_exists('get_field')){
		if(get_field('nrc_service_slug')){
			echo "<script src='https://transparency.nrchealth.com/widget/v2/vancouver-clinic/lotw.js' async></script>";
		}
	}
});
