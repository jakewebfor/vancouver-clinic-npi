
window.DS_OPT = {
    
    /**
     * This function will override the default builder function and
     * will be used to construct the HTML markup for the ds-summary placeholder
     * 
     * arguments
     *   data : the profile data returned from the api call (see API documentation for more information)
     *   ctx  : contains information about the placeholder element and a template helper function
     */	
     
     buildSummaryHTML: function(data, ctx){
         
         // a variable to hold the HTML markup
         var x;
 
         // make sure we have data and it is valid
         if(data && data.valid){
             // grab the profile data
             DS_OPT.commentCount = data.profile.bodycount;
             var profile = data.profile;
             if(profile){
                 // setup the variables that the template will need
                 var avgRating = profile.averageRating;
                 var starWidth = Math.round(avgRating / 5 * 100);

                 var templateData = {
                         stars:            profile.averageStarRatingStr,	
                         rating:           profile.averageRatingStr,
                         ratingCnt:        profile.reviewcount,
                         ratingCntLabel:   profile.reviewCountLabel,
                         commentCnt:       profile.bodycount,
                         commentCntLabel:  profile.bodyCountLabel,
                         profileName:	  profile.name,
                         percentStars:		starWidth	
                 }; 
                 
                 // build the HTML markup using {{var-name}} for the template variables
                 var template = [
                         '<div class="rating-bar"><div style="width: {{percentStars}}%"></div></div>',
                         '<div class="ds-ratings">(<span class="ds-ratingcount">{{ratingCnt}}</span> {{ratingCntLabel}})</div>',
                         '<div class="provider-header-info"><div class="ds-xofy"><span class="ds-average">{{rating}}</span><span class="ds-average-max"> out of 5</span></div>',
                         '<span> | </span>',
                         '<div class="ds-comment"><a href="#nrc-comments"><span class="ds-commentcount">{{commentCnt}}</span> {{commentCntLabel}}</a></div></div>',
             
             '<script type="application/ld+json">{',
                                 ' "@context": "http://schema.org",',
                                 '"@type": "Physician",',
                                 '"name": "{{profileName}}",',
                                 '"image": "', php_vars.image, '",',
                                 '"aggregateRating": {',
                                     ' "@context": "http://schema.org",',
                                     '"@type": "AggregateRating",',
                                     '"ratingValue": "{{rating}}",',
                                     '"reviewCount": "{{ratingCnt}}"}',
                                 '}',
                             '<\/script>'
                 ].join('');
                 
                 x = ctx.tmpl(template, templateData);
 
 
             }
         }
        
        return x; 
    },

    initCommentsWidget: function(data, ctx) {
        //set variable to the profile's create date
        DS_OPT.commentCount = data.profile.bodycount;
        console.log(DS_OPT.commentCount);
        DS_OPT.minimumCommentCount = (DS_OPT.commentCount > 4);
        console.log(DS_OPT.minimumCommentCount);
        console.log(ctx);
    },

    buildCommentsPrefixHTML: function(data, ctx){
    
    	var x = '';
    	if(ctx.info.distribution && ctx.info.distribution >= 1 && ctx.info.distribution <= 5){
			x = x + '<span class="ds-filter-info">Showing '+ctx.info.distribution+' star comments - <span style="color:blue; cursor:pointer;" class="ds-filter-clear-btn" data-ds-rating="clear">show all</span></span>';
		}
    

    	return x;
    },
	
	
	    buildCommentsEntryHTML:function(data, ctx){

      var dateToShow = data.formattedReviewDate;
      var pubsOn = '';
      if(ctx.view === 'scheduled'){
          dateToShow = data.formattedPublishDate;
          pubsOn = '<span class="ds-pubs">Publishes On</span>';
      }

  var bodyMarkup = '';
  var bodyMarkup2 = '';
  var bodyMarkup3 = '';
  if(data.bodyForDisplay && data.bodyForDisplay.length > 0){
      bodyMarkup = '<p class="ds-body ds-body-full">'+data.bodyForDisplay+'</p>';
  }
   if(data.body2ForDisplay && data.body2ForDisplay.length > 0){
      bodyMarkup2 = '<p class="ds-body ds-body-full"><b>'+data.bodyText2+'</b><br>'+data.body2ForDisplay+'</p>';
   }
   if(data.body3ForDisplay && data.body3ForDisplay.length > 0){
      bodyMarkup3 = '<p class="ds-body ds-body-full"><b>'+data.bodyText3+'</b><br>'+data.body3ForDisplay+'</p>';
   }

   				var avgRating = data.rating;
				var starWidth = Math.round(avgRating / 5 * 100);
   
      var templateData = {
          stars:          data.rating,
          date:           dateToShow,
          author:         data.formattedDisplayName,
          provider:       data.profile.name,
		  bodyMarkup:     bodyMarkup,
		  bodyMarkup2:    bodyMarkup2,
		  bodyMarkup3:    bodyMarkup3,
          pubsOn:         pubsOn,
		  percentStars:	  starWidth,
          url:            data.profile.docprofileslug
      };

      var profileTemplate = [
               '<div class="ds-comment">',
                  '<div class="ds-lcol">',
                      '<div class="rating-bar"><div style="width: {{percentStars}}%"></div></div>',
                      '{{pubsOn}}',
                      '<span class="ds-date">{{date}}</span>',
                  '</div>',
                  '<div class="ds-rcol">',
            '{{bodyMarkup}}',
            '{{bodyMarkup2}}',
            '{{bodyMarkup3}}',
                  '</div>',
               '</div>',
               '<div class="ds-clear"></div>',
               '<hr class="ds-divider">'
      ].join('');

      var groupTemplate = [
               '<div class="ds-comment">',
                  '<div class="ds-lcol">',
                      '<span class="ds-stars ds-stars{{stars}}"></span>',
                      '{{pubsOn}}',
                      '<span class="ds-date">{{date}}</span>',
                  '</div>',
                  '<div class="ds-rcol">',
                                  '{{bodyMarkup}}',
								'{{bodyMarkup2}}',
								'{{bodyMarkup3}}',
                          '<span class="ds-provider"><span class="ds-provider-label">Review for</span> ',
                             '<a href="{{INDEX_URL}}/slug/{{url}}" target="_blank">{{provider}}</a>',
                          '</span>',
                  '</div>',
               '</div>',
               '<div class="ds-clear"></div>',
               '<hr class="ds-divider">'
      ].join('');


      if(ctx.info.view == 'profile'){
        return ctx.tmpl(profileTemplate, templateData);
        
      } else if(ctx.info.view == 'group'){
        return ctx.tmpl(groupTemplate, templateData);
      }

    },

    buildCommentsSuffixHTML: function(data, ctx){
        if (DS_OPT.commentCount > 0) {    
            return'<div class="ds-poweredby"><span>Ratings and Comments Powered by </span><a  target="_blank" href="https://nrchealth.com/platform/star-ratings/">National Research Corporation</a></div>';
        } else {
                return "This provider has no patient comments at this time.<br/><br/>";
            }  
        } 
 };

 //Add desired elements to DOM
 (function($){
    $(document).ready(function(){
        //Target DOM element you want to append NRC summary to  
        
        //Summary getting appended to Provider Title
        $('.provider-page-hero .page-section-content .vc_col-sm-12.wpb_column.column_container.height-full').append('<div class="ds-summary provider-header"></div>');

        //Append comments underneath Location
        $('.provider-single-bio-section > .vc_col-sm-6:first-child.wpb_column.column_container.height-full').append('<span id="nrc-comments"></span><section class="nrc-comments"><h3>Patient Comments</h3><div class="ds-comments" data-ds-pagesize="5"></div><a class="survey-link" href="/about-our-survey" target="_blank">About Our Survey</a></section>');
    })
})(jQuery)

