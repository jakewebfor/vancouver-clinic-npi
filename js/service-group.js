window.DS_OPT = {
    buildSummaryHTML:	function(data, ctx){

        var x;
      
        if(data){
          if(!data.valid){
            return '';
          }


          var group = data.group;

          if (group){
              // setup the variables that the template will need
              // setup the variables that the template will need
              var avgRating = group.averageRating;
              var starWidth = Math.round(avgRating / 5 * 100);	
              var templateData = {
                stars:            group.averageStarRatingStr,	
                rating:           group.averageRatingStr,
                ratingCnt:        group.reviewcount,
                ratingCntLabel:   group.reviewCountLabel,
                commentCnt:       group.bodycount,
                commentCntLabel:  group.bodyCountLabel,
                groupName:	  group.name,
                percentStars:		starWidth
            }; 
            // build the HTML markup using {{var-name}} for the template variables
            var template = [
                '<div class="rating-bar"><div style="width: {{percentStars}}%"></div></div><div class="ds-xofy"><span class="ds-average">{{rating}}</span><span class="ds-average-max">out of 5</span></div><div class="ds-ratings"><span class="ds-ratingcount">{{ratingCnt}}</span> {{ratingCntLabel}}</div><div class="ds-comments"><span class="ds-commentcount">{{commentCnt}}</span> {{commentCntLabel}}</div>',

                '<script type="application/ld+json">{',
                                 ' "@context": "http://schema.org",',
                                 '"@type": "Service",',
                                 '"provider": {',
                                    '"@type": "MedicalOrganization",',
                                    '"name": "The Vancouver Clinic"',
                                 '},',
                                 '"name": "{{groupName}}",',
                                 '"aggregateRating": {',
                                     ' "@context": "http://schema.org",',
                                     '"@type": "AggregateRating",',
                                     '"ratingValue": "{{rating}}",',
                                     '"reviewCount": "{{ratingCnt}}"}',
                                 '}',
                             '<\/script>'
            ].join('')
          }
          
          x = ctx.tmpl(template, templateData);
        }
      
        return x;
      }
};

//Generate Schema Markup

 //Add desired elements to DOM
 (function($){
    $(document).ready(function(){
        //Target DOM element you want to append NRC summary to  
        
        //Summary getting appended to Service Title
        $('.provider-page-hero .page-section-content .vc_col-sm-12.wpb_column.column_container.height-full').append('<div class="ds-summary service-header" data-ds-view="group" data-ds-id="' + php_vars.service + '"></div>');

    })
})(jQuery)

